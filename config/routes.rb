Rails.application.routes.draw do

  devise_for :clients, controllers: {
               registrations: 'clients/registrations'
             }

  get 'operations/check_balance', to: 'operations#check_balance', as: 'check_balance'
  get 'operations/transaction', to: 'operations#transaction', as: 'transaction'
  post 'operations/create_transfer', to: 'operations#create_transfer', as: 'create_transfer'

  root to: 'home#index'
end

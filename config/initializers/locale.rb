I18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]

I18n.available_locales = [:pt_br, :en]

I18n.default_locale = :pt_br

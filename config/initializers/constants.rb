module BankAccounting
  module Constants
    # buttons_to helper
    BUTTONS = {
      add: { icon: :plus, btn: 'btn-primary' },
      edit: { icon: :pencil, btn: 'btn-default' },
      show: { icon: :info, btn: 'btn-default' },
      back: { icon: 'arrow-left', btn: 'btn-default' },
      delete: { icon: 'times', btn: 'btn-danger' },
      sign_in: { icon: 'sign-in', btn: 'btn-default' },
      sign_out: { icon: 'sign-out', btn: 'btn-default' }
    }.freeze

    FLASH_MESSAGES = {
      notice: { icon: 'info', body: 'success' },
      alert: { icon: 'times', body: 'danger' },
      error: { icon: 'times', body: 'danger' }
    }.freeze
  end

end

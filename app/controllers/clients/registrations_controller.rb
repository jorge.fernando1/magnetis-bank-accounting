class Clients::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  def create
    resource = build_resource(sign_in_params)
    resource.save
    sign_in(resource)
    respond_with resource, location: -> { root_path }
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end


  private

  def sign_in_params
    allow = [:name, :email, :password, :password_confirmation, [account_attributes: [:amount]]]
    params.require(resource_name).permit(allow)
  end
end

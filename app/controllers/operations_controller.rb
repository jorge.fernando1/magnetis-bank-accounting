class OperationsController < ApplicationController
  def check_balance
  end

  def transaction
    @params = {}
  end

  def create_transfer
    source_id = current_client.account.id
    destination_id = transaction_params[:target]
    amount = transaction_params[:amount]
    @response = CreateTransaction.create(source_id, destination_id, amount)

    if @response.valid?
      flash[:notice] = 'Transferencia realizada com sucesso.'
      redirect_to root_path
    else
      render :transaction
    end
  end


  def transaction_params
    return @params if @params
    @params = params.require(:transfer).permit(:target, :amount)
    @params
  end
end

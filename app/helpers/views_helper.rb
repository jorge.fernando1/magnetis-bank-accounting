module ViewsHelper
  def icon(icon, text, options = {})
    options[:class] = options[:class].present? ? "#{options[:class]} fa-fw" : 'fa-fw'
    fa_icon icon, options.merge(text: text)
  end

  def model_name(klass, type = :singular)
    count = type == :singular ? 1 : 2
    klass.model_name.human(count: count)
  end

  def model_with_icon(klass, type = :singular, options = {})
    icon klass.icon, model_name(klass, type), options
  end
end

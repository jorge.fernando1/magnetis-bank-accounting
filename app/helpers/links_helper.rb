module LinksHelper
  BankAccounting::Constants::BUTTONS.each do |type, parameters|
    define_method "link_to_#{type}" do |url, options = {}|
      options['data-container'] = 'body'
      label = options.include?(:label) ? options[:label] : t("buttons.#{type}")
      options[:title] = label.nil? || label.empty? ? t("buttons.#{type}") : nil
      link_icon = get_icon(parameters[:icon], label, options)

      unless options.delete(:only_link)
        options[:class] ||= ''
        options[:class] << " tip btn #{parameters[:btn]}"
      end

      link_to(url, options) { link_icon }
    end
  end

  private

  def get_icon(name, text, options = {})
    right = options.delete(:right)
    icon(name, text, right: right)
  end
end

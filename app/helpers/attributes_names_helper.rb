module AttributesNamesHelper

  def show_attribute_info(model, attribute, options = {})
    case options[:as]
    when :date then show_date_info(model, attribute, options)
    else show_text_info(model, attribute, options)
    end
  end


  def attribute_name_for(klass, attribute)
    klass.human_attribute_name(attribute)
  end

  def attributes_name_for(klass, attributes, &block)
    names = attributes.map { |attr| klass.human_attribute_name(attr) }
    names.each do |attribute|
      yield(attribute)
    end
  end

  private

  def show_text_info(model, attribute, options)
    dt = content_tag :dt, attribute_name_for(model.class, attribute)
    dd = content_tag :dd, model.send(attribute), class: options[:dl_class]

    dt + dd
  end

  def show_date_info(model, attribute, options)
    dt = content_tag :dt, attribute_name_for(model.class, attribute)
    dd = content_tag :dd, class: options[:dl_class] do
      I18n.l model.send(attribute), format: options[:format]
    end
    dt + dd
  end
end

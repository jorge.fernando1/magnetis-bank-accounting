module FormsHelper
  def title_error_for(model)
    model_name = model.model_name.human
    count = model.errors.count
    word = I18n.t('words.error', count: count)
    "#{model_name}: #{count} #{word}"
  end
end

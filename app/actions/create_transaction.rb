class CreateTransaction
  def self.create(source_id, destination_id, amount)
    this = self.new(source_id, destination_id, amount)
    this.create_transaction
  end

  def initialize(source_id, destination_id, amount)
    @response = TransactionResponse.new
    @source = find_account(source_id, :source)
    @destination = find_account(destination_id, :destination)
    @amount = check_and_parse_amount(amount)
  end

  def create_transaction
    return @response unless transaction_is_valid?
    ActiveRecord::Base.transaction do
      withdraw_from_source
      deposit_on_destination
    end
    @response
  end

  private

  def withdraw_from_source
    @source.amount -= @amount.to_money
    @source.save!
    create_transaction_for :debit, from: @source, to: @destination, amount: @amount
  end

  def deposit_on_destination
    @destination.amount += @amount.to_money
    @destination.save!
    create_transaction_for :credit, from: @destination, to: @source, amount: @amount
  end

  def create_transaction_for(type, options)
    Transaction.new.tap do |account|
      account.type_transaction = type
      account.source_account = options[:from]
      account.destination_account = options[:to]
      account.amount = options[:amount].to_money
      account.save!
    end
  end

  def find_account(id, type)
    return @response.add_error(type, :blank) unless id

    account = Account.find_by(id: id)
    @response.add_error(type, :not_found) unless account
    account
  end

  def transaction_is_valid?
    check_source_has_enough_funds
    @response.valid? ? true : false
  end

  def check_and_parse_amount(amount)
    parsed_amount = MoneyUtils.parse(amount, 'BRL')
    @response.add_error(:amount, :invalid) if !amount || parsed_amount <= 0
    parsed_amount
  end

  def check_source_has_enough_funds
    return if @response.invalid?
    if (@source.amount - @amount) < Money.new(0, 'BRL')
      @response.add_error(:source, :doesnt_have_enough_funds)
    end
  end
end

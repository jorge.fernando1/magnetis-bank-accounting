class TransactionResponse
  I18N_PATH = 'actions.transfer.errors'

  def initialize
    @error_base = {}
  end

  def errors
    return [] if @error_base.empty?
    @error_base.map { |key, value| { key => value } }
  end

  def add_error(field, message)
    return unless field
    return if message.nil? || message.empty?
    add_error_in_hash(field, message)
  end

  def valid?
    @error_base.empty?
  end

  def invalid?
    !self.valid?
  end

  def include?(key)
    @error_base.include? key.to_sym
  end

  def clear!
    @error_base.clear
  end

  def full_messages
    return [] if @error_base.empty?
    messages = []
    @error_base.each do |key, errors|
      errors.each { |error| messages << error_message(key, error) }
    end
    messages
  end

  private

  def add_error_in_hash(field, message)
    f = field.to_sym
    if @error_base.include?(f)
      @error_base[f] << message.to_s
    else
      @error_base[f] = [ message.to_s ]
    end
  end

  def error_message(key, error)
    key_i18n = translate_key(key)
    msg = translate_message(key, error)
    "#{key_i18n} #{msg}"
  end

  def translate_key(key)
    I18n.t(I18N_PATH + ".keys.#{key}", default: key)
  end

  def translate_message(key, error)
    msg = I18n.t(I18N_PATH + ".#{key}.#{error}", default: nil)
    msg ||= I18n.t(I18N_PATH + ".#{error}", default: error)
    msg
  end
end

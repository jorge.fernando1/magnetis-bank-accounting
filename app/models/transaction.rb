class Transaction < ApplicationRecord
  TYPE_I18N_PATH = 'enums.transaction.type_transaction'.freeze

  monetize :amount_cents
  enum type_transaction: { debit: 0, credit: 1 }

  belongs_to :source_account, class_name: 'Account' ,foreign_key: 'source_account'
  belongs_to :destination_account, class_name: 'Account', foreign_key: 'destination_account', optional: true

  def type_transaction_translated
    I18n.t(TYPE_I18N_PATH + self.type)
  end

  def self.type_transaction_values
    self.types.map do |key, _value|
      [I18n.t(TYPE_I18N_PATH + key), key]
    end
  end
end

class Account < ApplicationRecord
  monetize :amount_cents

  has_many :transactions, foreign_key: 'source_account'
  belongs_to :client
end

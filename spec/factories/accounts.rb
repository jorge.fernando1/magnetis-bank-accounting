FactoryGirl.define do
  factory :account do
    amount { Money.new(1000, 'BRL') }
    client { create(:client) }
  end
end

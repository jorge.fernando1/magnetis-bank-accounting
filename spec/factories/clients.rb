FactoryGirl.define do
  factory :client do
    sequence(:name) { |n| "Nice Client Name #{n}" }
    sequence(:email) { |n| "client#{n}@client.com" }
    password { '123456789' }
    # account { create(:account) }
  end
end

require 'rails_helper'

RSpec.describe TransactionResponse do
  let(:response) { described_class.new }

  describe '.errors' do
    context 'when are not errors' do
      it 'return a empty erray' do
        expect(response.errors).to eq []
      end
    end

    context 'when are errors' do
      it 'return a array of hashes - 1 error' do
        response.add_error(:base, :blank)
        expect(response.errors).to eq [{ base: ['blank'] }]
      end

      it 'return a array of hashes - many errors' do
        array_test = [ { base: ['blank'] }, { other: ['blank'] },
                       { another: ['blank'] } ]
        response.add_error(:base, :blank)
        response.add_error(:other, :blank)
        response.add_error(:another, :blank)
        expect(response.errors.size).to eq 3
        expect(response.errors).to eq array_test
      end
    end
  end


  describe '.add_error' do
    context 'dont add anything' do
      it 'when key is nil' do
        response.add_error(nil, 'invalid')
        expect(response.errors).to be_empty
      end

      it 'when message is nil' do
        response.add_error(:base, nil)
        expect(response.errors).to be_empty
      end

      it 'when message is empty' do
        response.add_error(:base, '')
        expect(response.errors).to be_empty
      end

      it 'when errors is already added' do
        response.add_error(:base, :blank)
        response.add_error(:base, :blank)
        expect(response.errors.size).to eq 1
      end

    end

    context 'when error is valid' do
      context 'when add error for differents keys' do
        it 'add on array' do
          response.add_error(:base, :blank)
          errors = response.errors
          expect(errors).to_not be_empty
          expect(errors.size).to eq 1
          expect(errors).to eq([{ base: ['blank'] }])
        end

        it 'add various errors on array' do
          response.add_error(:base, :blank)
          response.add_error(:other, :blank)
          response.add_error(:another, :blank)

          errors = response.errors
          expect(errors).to_not be_empty
          expect(errors.size).to eq 3
        end
      end

      context 'when add errors to same key' do
        it 'append new error to array' do
          response.add_error(:base, :blank)
          response.add_error(:base, :invalid)

          error = response.errors.first

          expect(response.errors.size).to eq 1
          expect(error[:base].size).to eq 2
          expect(error[:base]).to eq ['blank', 'invalid']
        end

        it 'append errors to correct key' do
          array_test = [
            { base: ['blank', 'invalid'] }, { other: ['foo', 'bar'] }
          ]
          response.add_error(:base, :blank)
          response.add_error(:base, :invalid)
          response.add_error(:other, :foo)
          response.add_error(:other, :bar)

          errors = response.errors
          expect(errors).to eq array_test
        end
      end

      context 'when a key is a String' do
        it 'add the error and convert key to symbol' do
          array_text = [{ base: ['blank'] }]

          response.add_error('base', :blank)
          expect(response.errors).to eq array_text
        end

        it 'add the error to correct key' do
          array_test = [ { base: ['blank', 'invalid'] } ]
          response.add_error(:base, :blank)
          response.add_error('base', :invalid)
          expect(response.errors).to eq array_test
        end
      end

    end
  end

  describe '.valid?' do
    it 'return true when are no errors' do
      response.clear!
      expect(response).to be_valid
    end

    it 'return false when are errors' do
      response.add_error(:base, :foo)
      expect(response).to_not be_valid
    end
  end

  describe '.invalid?' do
    it 'return true when are errors' do
      response.add_error(:base, :foo)
      expect(response).to be_invalid
    end

    it 'return false when are no errors' do
      response.clear!
      expect(response).to_not be_invalid
    end
  end

  describe '.include?' do
    context 'when param is a symbol' do
      it 'return true when has an error' do
        response.add_error(:foo, 'foo_error')
        expect(response).to include(:foo)
      end

      it 'return false when doesnt have an error' do
        response.add_error(:foo, 'foo-error')
        expect(response).to_not include(:bar)
      end
    end

    context 'when param is a string' do
      it 'return true when has an error' do
        response.add_error(:foo, 'foo_error')
        expect(response).to include('foo')
      end

      it 'return false when doesnt have an error' do
        response.add_error(:foo, 'foo-error')
        expect(response).to_not include('bar')
      end
    end
  end


  describe '.full_messages' do
    before(:each) do
      file = Rails.root.join('spec', 'fixtures', 'transaction_translations.yml')
      I18n.backend.store_translations(:test,
        YAML.load_file(File.open(file))['test']
      )
      I18n.available_locales << :test
      I18n.locale = :test
    end

    context 'when hasn\'t errors' do
      it 'return an empty array' do
        msgs = response.full_messages
        expect(msgs).to eq []
      end
    end

    context 'when has errors' do
      context 'when a key contain only 1 error' do
        it 'create one message error' do
          msg = ['Nice foo and Nice Bar']
          response.add_error(:foo, :bar)
          expect(response.full_messages).to eq msg
        end
      end

      context 'when a key contain 2 or more errors' do
        it 'create different messages' do
          msgs = ['Nice foo and Nice Bar', "Nice foo Can't be blank"]
          response.add_error(:foo, :bar)
          response.add_error(:foo, :blank)

          expect(response.full_messages).to eq msgs
        end
      end

      context 'when 2 keys contains 1 error:' do
        it 'create 2 messages' do
          msgs = ['Nice foo and Nice Bar', 'Account Base bar']
          response.add_error(:foo, :bar)
          response.add_error(:base, :bar)
          expect(response.full_messages).to eq msgs
        end
      end

      context 'when 2 keys contains 2 errors:' do
        it 'create 4 messages' do
          msgs = ['Nice foo and Nice Bar', "Nice foo Can't be blank",
                  'Account Base bar', "Account Can't be blank"]
          response.add_error(:foo, :bar)
          response.add_error(:foo, :blank)
          response.add_error(:base, :bar)
          response.add_error(:base, :blank)
          expect(response.full_messages).to eq msgs
        end
      end

    end
  end

end

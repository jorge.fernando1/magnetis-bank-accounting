require 'rails_helper'

RSpec.describe CreateTransaction do
  let(:source_account) { create(:account) }
  let(:destination_account) { create(:account) }
  let(:source_id) { source_account.id }
  let(:destination_id) { destination_account.id }

  describe '.create' do
    context 'dont create a transaction' do
      it 'when source account is nil' do
        response = described_class.create(nil, destination_id, 100)
        expect(response).to be_invalid
        expect(response).to include(:source)
      end

      it 'when source account doesnt exists' do
        response = described_class.create(99999, destination_id, 100)
        expect(response).to be_invalid
        expect(response).to include(:source)
      end


      it 'when destination account is nil' do
        response = described_class.create(source_id, nil, 100)
        expect(response).to be_invalid
        expect(response).to include(:destination)
      end

      it 'when destination account doesnt exists' do
        response = described_class.create(source_id, 99999, 100)
        expect(response).to be_invalid
        expect(response).to include(:destination)
      end

      it 'when amount is nil' do
        response = described_class.create(source_id, destination_id, nil)
        expect(response).to be_invalid
        expect(response).to include(:amount)
      end

      it 'when amount is equal zero' do
        response = described_class.create(source_id, destination_id, 0)
        expect(response).to be_invalid
        expect(response).to include(:amount)
      end

      it 'when amount is less than zero' do
        response = described_class.create(source_id, destination_id, -100)
        expect(response).to be_invalid
        expect(response).to include(:amount)
      end

      it 'when source account doesnt have enought funds', test: true do
        source_account.update(amount: 10.to_money)
        response = described_class.create(source_id, destination_id, 100)
        error = response.errors.first

        expect(response).to be_invalid
        expect(response).to include(:source)
        expect(error[:source]).to eq ['doesnt_have_enough_funds']
      end
    end

    context 'When do a transaction' do
      before(:each) do
        source_account.update(amount: 100.to_money)
        destination_account.update(amount: 100.to_money)

        @response = described_class.create(source_id, destination_id, 50)
        expect(@response).to be_valid
        source_account.reload
        destination_account.reload
      end

      context 'source account:' do
        it 'subtract amount' do
          expect(source_account.amount).to eq 50.to_money
        end

        it 'create a new transaction record' do
          transactions = source_account.transactions
          expect(transactions.count).to eq 1

          transaction = transactions.first
          expect(transaction.type_transaction).to eq 'debit'
          expect(transaction.source_account).to eq source_account
          expect(transaction.destination_account).to eq destination_account
          expect(transaction.amount).to eq 50.to_money
        end
      end

      context 'destination account:' do
        it 'sum amount' do
          expect(destination_account.amount).to eq 150.to_money
        end

        it 'create a new transaction record' do
          transactions = destination_account.transactions
          expect(transactions.count).to eq 1

          transaction = transactions.first
          expect(transaction.type_transaction).to eq 'credit'
          expect(transaction.source_account).to eq destination_account
          expect(transaction.destination_account).to eq source_account
          expect(transaction.amount).to eq 50.to_money
        end
      end
    end
  end
end

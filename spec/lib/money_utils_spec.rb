require 'rails_helper'

RSpec.describe MoneyUtils do

  describe '.parse' do
    let(:util) { described_class }

    context 'return 0' do
      it 'when value is nil' do
        value = util.parse(nil, 'BRL')
        expect(value).to eq 0.to_money
      end
    end

    context 'nicely parse classes: ' do
      context 'String. ' do
        let(:test_value) { Money.new(10000, 'BRL') }

        it 'when is 100' do
          value = util.parse('100', 'BRL')
          expect(value).to eq test_value
        end

        it 'when is 100.00' do
          value = util.parse('100.00', 'BRL')
          expect(value).to eq test_value
        end

        it 'when 100,00' do
          value = util.parse('100,00', 'BRL')
          expect(value).to eq test_value
        end
      end
    end

  end
end

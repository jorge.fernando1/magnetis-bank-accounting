class CreateAccount < ActiveRecord::Migration[5.1]
  def change
    create_table :accounts do |t|
      t.monetize :amount, default: 0
      t.belongs_to :client
      t.timestamps
    end
  end
end

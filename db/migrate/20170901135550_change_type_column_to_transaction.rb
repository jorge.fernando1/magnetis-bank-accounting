class ChangeTypeColumnToTransaction < ActiveRecord::Migration[5.1]
  def change
    change_table :transactions do |t|
      t.rename :type, :type_transaction
    end
  end
end

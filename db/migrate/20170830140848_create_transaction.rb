class CreateTransaction < ActiveRecord::Migration[5.1]
  def change
    create_table :transactions do |t|
      t.integer :type
      t.monetize :amount
      t.integer :source_account
      t.integer :destination_account
      t.foreign_key :accounts, column: :source_account, null: false
      t.foreign_key :accounts, column: :destination_account, null: true
      t.timestamps
    end
  end
end

class MoneyUtils
=begin
  Parse value and convert to Money class
  Easy way to convert numbers to correct value.

  For example:  $ 100.00
  You can pass the following values
  - 100.00
  - 100,00
  - 100
  And we get the same Money Value

  When value is nil
=end
  @dots_regex = /(\,|\.)/

  def self.parse(value, currency)
    return Money.new(0, currency) unless value
    split = value.to_s.split(@dots_regex)
    if split.size == 1
      return Money.new( split.first + '00', currency)
    else
      parsed = value.gsub(@dots_regex, '')
      return Money.new parsed, currency
    end
  end

end

# BANK ACCOUNTING SYSTEM

This project is a bank simulator.

Consist of a Clients that have one Account, and can make operations with that.

For now, to system has two operations:
- Check the balance
- Transfer cash to another count.


## Check Balance
 Is just a Rails page, nothing difficult.

## Transfer Cash

To transfer cash, we use an Action called `create_transaction`, that checks
if the accounts exists, if the source account has the money to tranfers.
And respond with an instance of `TransactionResponse` that if the transaction are valid
or not, and its errors.

## Test

Just run `rspec`, but for some reason, the tests of `transaction_response_spec.rb` dont pass when you execute `rspec`, but passses if you execute only the file.
